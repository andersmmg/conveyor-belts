

<h1>Conveyor Belts (Fabric)</h1>

![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_belts_side_view.mp4)

<h2>About</h2>

CurseForge: https://www.curseforge.com/minecraft/mc-mods/conveyor-belts-fabric

Modrinth: https://modrinth.com/mod/conveyor-belts

This mod adds 3 tiers of belts. Wood, Iron and Gold.

![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/redstone_inv.png)

<h2>Tiers</h2>

Wood, Iron and Gold (left to right)
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/belt_types.png)

Slope up:
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/belt_slope_up_types.png)

Filters:
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/belt_filter_types.png)

<h2>Redstone</h2>


Belts can be disabled with a redstone input:
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_belts_redstone.mp4)

<h2>Filtering</h2>

With no filters set, items will be split evenly between the two outputs:
<br />

![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_no_filter.mp4)

With a filter set on one side, that item will go to that output and all other items will go to the other one
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_single_filter.mp4)

With both filters set, items will go to the output with the filter on, if the item matches neither filter a sound will be played to indicate it is jammed.
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_double_filter.mp4)

<h2>Placement</h2>

Belts will face away from you when placed. Holding shift will make them face towards you.
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_shift_placement.mp4)

<h2>Furnace array demo</h2>

![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_furnace_demo_setup.mp4)
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_furnace_demo.mp4)
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/conveyor_furnace_demo_backed_up.mp4)

<h2>Misc notes</h2>
The mod preforms well with lots of belts in the world.
<br />
![](https://gitlab.com/billyg270/conveyor-belts/-/raw/master/readme_assets/stress_test.png)



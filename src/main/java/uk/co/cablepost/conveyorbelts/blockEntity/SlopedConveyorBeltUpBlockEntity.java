package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.block.ConveyorBelt;
import uk.co.cablepost.conveyorbelts.block.SlopedConveyorBeltUp;

import java.util.Objects;

public class SlopedConveyorBeltUpBlockEntity extends ConveyorBeltBlockEntity {

    public Boolean copiedItemsFromBelowIfTop = false;

    public SlopedConveyorBeltUpBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(pos, state, blockEntityType);
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, SlopedConveyorBeltUpBlockEntity blockEntity) {

        boolean top = state.get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER;

        if(!blockEntity.copiedItemsFromBelowIfTop && top) {
            try {
                SlopedConveyorBeltUpBlockEntity be = ((SlopedConveyorBeltUpBlockEntity) Objects.requireNonNull(world.getBlockEntity(pos.add(0, -1, 0))));
                blockEntity.items = be.items;
                blockEntity.transferCooldownCounter = be.transferCooldownCounter;
                blockEntity.transferSidewaysOffset = be.transferSidewaysOffset;
            }
            catch(java.lang.NullPointerException e){
                //other half of sloped conveyor is not below
                blockEntity.copiedItemsFromBelowIfTop = true;//just do this to stop it trying over and over
                return;
            }
            blockEntity.copiedItemsFromBelowIfTop = true;
        }

        if(!state.get(ConveyorBelt.ENABLED) || top){
            blockEntity.updateSlotActuallyEmptyHack();
            return;
        }

        for(int i = 0; i < 3; i++) {
            if(blockEntity.getStack(i).isEmpty()){
                //if empty slot, reset cool-downs
                blockEntity.resetCooldowns(i);
                continue;
            }

            blockEntity.updateCooldowns(i);

            //if cool-down has run down, meaning item can goto next slot
            if (blockEntity.transferCooldownCounter[i] <= 0) {
                blockEntity.transferCooldownCounter[i] = 0;

                if(i == 2){
                    //if last slot in belt, will goto next belt
                    Direction direction = state.get(ConveyorBelt.FACING);
                    blockEntity.moveToNextBeltSloped(direction, i, true);
                }
                else {
                    //else, moving from one slot to another within this belt

                    if(transferToEmpty(blockEntity, i, blockEntity, i + 1)){
                        blockEntity.transferCooldownCounter[i + 1] = blockEntity.transferCooldown;
                        blockEntity.transferSidewaysOffset[i + 1] = blockEntity.transferSidewaysOffset[i];
                        blockEntity.markDirty();
                    }
                }
            }
        }

        blockEntity.updateSlotActuallyEmptyHack();
    }

    public boolean moveToNextBeltSloped(Direction direction, int fromSlot, boolean up){
        ConveyorBeltBlockEntity conveyorBlockEntityInfront = getConveyorBlockEntityAt(world, pos.offset(direction).add(0, (up ? 1 : 0), 0));

        return moveToNextBelt(direction, fromSlot, conveyorBlockEntityInfront);
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(!copiedItemsFromBelowIfTop && getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
            return false;//Prevents any items going into the top inventory before it is overwritten by the bottom one
        }

        return super.canInsert(slot, stack, dir);
    }

    @Override
    public boolean canMoveHereFromSide(World world, int sideIndex){
        if(getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
            return false;
        }
        return super.canMoveHereFromSide(world, sideIndex);
    }

    @Override
    public void markDirty() {

        if(getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
            try {
                SlopedConveyorBeltUpBlockEntity be = ((SlopedConveyorBeltUpBlockEntity) Objects.requireNonNull(world.getBlockEntity(pos.add(0, -1, 0))));
                be.markDirty();
            }
            catch(java.lang.NullPointerException ignored){
            }
        }

        super.markDirty();
    }
}

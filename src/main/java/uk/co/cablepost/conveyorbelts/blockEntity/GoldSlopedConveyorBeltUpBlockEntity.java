package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public class GoldSlopedConveyorBeltUpBlockEntity extends SlopedConveyorBeltUpBlockEntity {
    public GoldSlopedConveyorBeltUpBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY);
        this.transferCooldown = ConveyorBelts.GOLD_TRANSFER_COOLDOWN;
        this.moveToCenterSpeed = ConveyorBelts.GOLD_MOVE_TO_CENTER_SPEED;
    }
}

package uk.co.cablepost.conveyorbelts.robotic_arm.filter;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreenHandler;
import uk.co.cablepost.conveyorbelts.util.ItemFilters;

public class FilterRoboticArmScreenHandler extends RoboticArmScreenHandler {
    public FilterRoboticArmScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(2), new ArrayPropertyDelegate(RoboticArmBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    public FilterRoboticArmScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(ConveyorBelts.FILTER_ROBOTIC_ARM_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);


        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        this.addSlot(new Slot(inventory, 0, 80, 35));
        this.addSlot(new Slot(inventory, 1, 34, 35));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 84 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 142));
        }
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {
        if (invSlot == 1) {
            return ItemStack.EMPTY;
        }

        return super.transferSlot(player, invSlot);
    }

    @Override
    public void onSlotClick(int slotIndex, int button, SlotActionType actionType, PlayerEntity player) {

        boolean doFilterSet = false;

        if(slotIndex == 1){
            Slot slot = this.slots.get(slotIndex);
            ItemStack newStack = getCursorStack().copy();

            if(!ItemFilters.IsFilterItem(newStack)) {//Filter items keep their NBT
                newStack = new ItemStack(newStack.getItem());//Using the item instead of the item stack to show the user this ignores things like damage, stack size and enchants - for non filter items
            }

            NbtCompound newNbt = newStack.getOrCreateNbt();
            newNbt.putBoolean("IsFilterItem", true);
            newStack.writeNbt(newNbt);
            slot.setStack(newStack);

            slot.markDirty();
            return;
        }

        super.onSlotClick(slotIndex, button, actionType, player);
    }
}

package uk.co.cablepost.conveyorbelts.robotic_arm.base.util;

import net.minecraft.item.ItemStack;

public class GetCanMoveResp {
    public int canMove;
    public ItemStack itemStackToInsertOn;
    public int slot;

    public GetCanMoveResp(){
        canMove = 0;
    }

    public GetCanMoveResp(int canMove, ItemStack itemStackToInsertOn, int slot){
        this.canMove = canMove;
        this.itemStackToInsertOn = itemStackToInsertOn;
        this.slot = slot;
    }
}

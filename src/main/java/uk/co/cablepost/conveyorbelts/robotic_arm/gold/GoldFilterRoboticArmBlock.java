package uk.co.cablepost.conveyorbelts.robotic_arm.gold;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmBlock;

public class GoldFilterRoboticArmBlock extends FilterRoboticArmBlock {

    public GoldFilterRoboticArmBlock(Settings settings) {
        super(settings, false);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldFilterRoboticArmBlockEntity(pos, state);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                checkType(type, ConveyorBelts.GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, GoldFilterRoboticArmBlockEntity::clientTick) :
                checkType(type, ConveyorBelts.GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, GoldFilterRoboticArmBlockEntity::serverTick)
        ;
    }
}

package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.IronSlopedConveyorBeltDownBlockEntity;

public class IronSlopedConveyorBeltDown extends SlopedConveyorBeltDown {
    public IronSlopedConveyorBeltDown(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronSlopedConveyorBeltDownBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? checkType(type, ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, IronSlopedConveyorBeltDownBlockEntity::clientTick) : checkType(type, ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, IronSlopedConveyorBeltDownBlockEntity::serverTick);
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
        vector = vector.multiply(ConveyorBelts.IRON_ENTITY_MOVE_SPEED);
        moveEntityOn2(vector, livingEntity);
    }

    public Identifier getNextCycleBeltType(){
        return ConveyorBelts.IRON_CONVEYOR_BELT_IDENTIFIER;
    }
}
